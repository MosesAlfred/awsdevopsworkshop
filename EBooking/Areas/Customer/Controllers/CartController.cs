﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using EBooking.DataAccess;
using EBooking.Models;
using System.Security.Claims;
using EBooking.Utilities;
using Stripe.Checkout;

namespace EBooking.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize]
    public class CartController : Controller
    {
        public IEBookingDbRepositories _db;
        [BindProperty]
        public ShoppingCartVM shoppingCartVM { get; set; }
        public CartController(IEBookingDbRepositories db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            shoppingCartVM = new ShoppingCartVM()
            {
                ListCart = _db.ShoppingCartRep.GetAll(u => u.ApplicationUserId == claim.Value, includeProperties: "Product"),
                OrderHeader = new()
            };
            foreach (var cart in shoppingCartVM.ListCart)
            {
                cart.Price = GetPriceBasedOnQuantity(cart.Count, cart.Product.Price, cart.Product.Price50, cart.Product.Price100);
                shoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
            }
            return View(shoppingCartVM);
        }
        public IActionResult Summary()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            shoppingCartVM = new ShoppingCartVM()
            {
                ListCart = _db.ShoppingCartRep.GetAll(u => u.ApplicationUserId == claim.Value, includeProperties: "Product"),
                OrderHeader = new()
            };

            var applicationUser = _db.ApplicationUserRep.GetFirstOrDefault(u => u.Id == claim.Value);
            shoppingCartVM.OrderHeader.ApplicationUser = applicationUser;

            shoppingCartVM.OrderHeader.StreetAddress = applicationUser.StreetAddress;
            shoppingCartVM.OrderHeader.City = applicationUser.City;
            shoppingCartVM.OrderHeader.PostalCode = applicationUser.PostalCode;
            shoppingCartVM.OrderHeader.Name = applicationUser.Name;
            shoppingCartVM.OrderHeader.State = applicationUser.State;
            shoppingCartVM.OrderHeader.PhoneNumber = applicationUser.PhoneNumber;


            foreach (var cart in shoppingCartVM.ListCart)
            {
                cart.Price = GetPriceBasedOnQuantity(cart.Count, cart.Product.Price, cart.Product.Price50, cart.Product.Price100);
                shoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
            }
            return View(shoppingCartVM);

        }
        [HttpPost]
        [ActionName("Summary")]
        [ValidateAntiForgeryToken]
        public IActionResult SummaryPost()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);

            shoppingCartVM.ListCart = _db.ShoppingCartRep.GetAll(u => u.ApplicationUserId == claim.Value, includeProperties: "Product");

            shoppingCartVM.OrderHeader.OrderDate = DateTime.Now;

            shoppingCartVM.OrderHeader.ApplicationUserId = claim.Value;
           

            foreach (var cart in shoppingCartVM.ListCart)
            {
                cart.Price = GetPriceBasedOnQuantity(cart.Count, cart.Product.Price, cart.Product.Price50, cart.Product.Price100);
                shoppingCartVM.OrderHeader.OrderTotal += (cart.Price * cart.Count);
            }

            var applicationUser = _db.ApplicationUserRep.GetFirstOrDefault(u => u.Id == claim.Value);
            if(applicationUser.CompanyId.GetValueOrDefault() == 0)
            {
                shoppingCartVM.OrderHeader.OrderStatus = SD.StatusPending;
                shoppingCartVM.OrderHeader.PaymentStatus = SD.PaymentStatusPending;
            }
            else
            {
                shoppingCartVM.OrderHeader.OrderStatus = SD.StatusApproved;
                shoppingCartVM.OrderHeader.PaymentStatus = SD.PaymentStatusForDelayedPayment;
            }
            _db.OrderHeaderRep.Add(shoppingCartVM.OrderHeader);
            _db.Save();

            foreach (var cartitem in shoppingCartVM.ListCart)
            {
                OrderDetail orderDetail = new()
                {
                    ProductId = cartitem.ProductId,
                    Count = cartitem.Count,
                    OrderId = shoppingCartVM.OrderHeader.Id,
                    Price = cartitem.Price
                };
                _db.OrderDetailRep.Add(orderDetail);
                _db.Save();
            }
            if (applicationUser.CompanyId.GetValueOrDefault() == 0)
            {
                #region "Stripe settings"
                var domain = "https://localhost:7017/";
                var options = new SessionCreateOptions
                {
                    //LineItems = new List<SessionLineItemOptions>
                    //{
                    //      new SessionLineItemOptions
                    //      {
                    //        PriceData = new SessionLineItemPriceDataOptions
                    //        {
                    //          UnitAmount = 2000,
                    //          Currency = "usd",
                    //          ProductData = new SessionLineItemPriceDataProductDataOptions
                    //          {
                    //            Name = "T-shirt",
                    //          },

                    //        },
                    //        Quantity = 1,
                    //      },
                    //},
                    LineItems = new List<SessionLineItemOptions>(),
                    Mode = "payment",
                    //SuccessUrl = "http://localhost:4242/success.html",
                    //CancelUrl = "http://localhost:4242/cancel.html",

                    SuccessUrl = domain + $"Customer/Cart/OrderConfirmation?id={shoppingCartVM.OrderHeader.Id}",
                    CancelUrl = domain + $"Customer/cart/Index",
                };
                foreach (var cart in shoppingCartVM.ListCart)
                {
                    var mysessionLineItem = new SessionLineItemOptions
                    {
                        PriceData = new SessionLineItemPriceDataOptions
                        {
                            UnitAmount = (long)(cart.Price * 100), //2000,
                            Currency = "usd",
                            ProductData = new SessionLineItemPriceDataProductDataOptions
                            {
                                Name = cart.Product.Title,
                            },

                        },
                        Quantity = cart.Count,
                    };
                    options.LineItems.Add(mysessionLineItem);
                }
                var service = new SessionService();
                Session session = service.Create(options);
                _db.OrderHeaderRep.UpdateStripePaymentId(shoppingCartVM.OrderHeader.Id, session.Id, session.PaymentIntentId);
                _db.Save();
                Response.Headers.Add("Location", session.Url);
                return new StatusCodeResult(303);

                #endregion "Stripe settings"
            }
            else
            {
                return RedirectToAction("OrderConfirmation", "Cart", new { id = shoppingCartVM.OrderHeader.Id });
            }
            //_db.ShoppingCartRep.RemoveRange(shoppingCartVM.ListCart);
            //_db.Save();
            //return RedirectToAction("Index", "Home");

        }

        public IActionResult OrderConfirmation(int id)
        {
            var orderFromDb =_db.OrderHeaderRep.GetFirstOrDefault(u=> u.Id == id);  
           
            if (orderFromDb != null)
            {
                if (orderFromDb.PaymentStatus != SD.PaymentStatusForDelayedPayment)
                {
                    var service = new SessionService();
                    Session session = service.Get(orderFromDb.SessionId);
                    if (session.PaymentStatus.ToLower() == "paid")
                    {
                        _db.OrderHeaderRep.UpdateStatus(id, SD.StatusApproved, SD.PaymentStatusApproved);
                        _db.Save();
                    }
                }
                var cartsFromDB = _db.ShoppingCartRep.GetAll(u => u.ApplicationUserId == orderFromDb.ApplicationUserId);
                if (cartsFromDB != null)
                {
                    _db.ShoppingCartRep.RemoveRange(cartsFromDB);
                    _db.Save();
                }
            }
            return View(id);
        }
        public IActionResult Plus(int cartId)
        {
            var cartFromDb = _db.ShoppingCartRep.GetFirstOrDefault(u => u.Id == cartId);
            _db.ShoppingCartRep.IncrementCount(cartFromDb, 1);
            _db.Save();
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Minus(int cartId)
        {
            var cartFromDb = _db.ShoppingCartRep.GetFirstOrDefault(u => u.Id == cartId);
            if (cartFromDb.Count <= 1)
            {
                _db.ShoppingCartRep.Remove(cartFromDb);
            }
            else
            {
                _db.ShoppingCartRep.DecrementCount(cartFromDb, 1);
            }
            _db.Save();
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Delete(int cartId)
        {
            var cartFromDb = _db.ShoppingCartRep.GetFirstOrDefault(u => u.Id == cartId);
            _db.ShoppingCartRep.Remove(cartFromDb);
            _db.Save();
            return RedirectToAction(nameof(Index));
        }
        private double GetPriceBasedOnQuantity(int quantity, double price, double price50, double price100)
        {
            if (quantity <= 50)
                return price;
            else if (quantity <= 100)
                return price50;
            else
                return price100;
        }
        //Get 

    }
}
