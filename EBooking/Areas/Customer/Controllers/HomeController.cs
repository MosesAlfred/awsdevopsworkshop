﻿using Microsoft.AspNetCore.Mvc;
using EBooking.DataAccess;
using EBooking.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace EBooking.Controllers;
[Area("Customer")]
public class HomeController : Controller
{
   // private readonly ILogger<HomeController> _logger;
    private readonly IEBookingDbRepositories _db;

    public HomeController(ILogger<HomeController> logger, IEBookingDbRepositories db)
    {
     //   _logger = logger;
        _db = db;
    }

    public IActionResult Index()
    {
        IEnumerable<Product> productList = _db.ProductRep.GetAll(includeProperties:"Category,CoverType");

        return View(productList);
    }

    public IActionResult Details(int productId)
    {
        ShoppingCart cartObj = new()
        {
            Count = 1,
            Product = _db.ProductRep.GetFirstOrDefault(p => p.Id == productId, includeProperties: "Category,CoverType"),
        };
        return View (cartObj);
    }
    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize]
    public IActionResult Details(ShoppingCart shoppingCart)
    {
        var claimsIdentity =(ClaimsIdentity)User.Identity;
        var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
        shoppingCart.ApplicationUserId = claim.Value;

        var cartFromDb = _db.ShoppingCartRep.GetFirstOrDefault(p => p.ApplicationUserId == claim.Value && p.ProductId == shoppingCart.ProductId);
        if (cartFromDb == null)
        {
            _db.ShoppingCartRep.Add(shoppingCart);
        }
        else
        {
            _db.ShoppingCartRep.IncrementCount(cartFromDb, shoppingCart.Count);
        }

        _db.Save();

        return RedirectToAction(nameof(Index));
    }

   
}
