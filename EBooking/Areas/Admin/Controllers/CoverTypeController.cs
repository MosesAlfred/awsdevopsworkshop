﻿using Microsoft.AspNetCore.Mvc;
using EBooking.DataAccess;
using EBooking.Models;

namespace EBooking.Controllers
{
    [Area("Admin")]
    public class CoverTypeController : Controller
    {
        private readonly IEBookingDbRepositories _dbRep;
        private readonly ICoverTypeRepository _coverTypeRep;
        public CoverTypeController(IEBookingDbRepositories dbRep)
        {
            _dbRep = dbRep;
            _coverTypeRep = _dbRep.CoverTypeRep;
        }

        public IActionResult Index()
        {
            var coverTypes = _coverTypeRep.GetAll();
            return View(coverTypes);
        }
        //Get
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CoverType obj)
        {
           
            if (ModelState.IsValid)
            {
                _coverTypeRep.Add(obj);
                _dbRep.Save();
                TempData["success"] = "CoverType created successfully";
                return RedirectToAction("Index");
            }

            return View(obj);
        }
        //Get
        public ActionResult Edit(int? id)
        {
            var coverType = _coverTypeRep.GetFirstOrDefault(c => c.Id == id);
            if (coverType == null)
            {
                return NotFound();
            }
            return View(coverType);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CoverType obj)
        {
            if (ModelState.IsValid)
            {
                _coverTypeRep.Update(obj);
                _dbRep.Save();
                TempData["success"] = "CoverType updated successfully";
                return RedirectToAction("Index");
            }

            return View(obj);
        }
        //Get
        public ActionResult Delete(int? id)
        {
            var coverType = _coverTypeRep.GetFirstOrDefault(c => c.Id == id);
            if (coverType == null)
            {
                return NotFound();
            }
            return View(coverType);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(int? id)
        {
            var coverType = _coverTypeRep.GetFirstOrDefault(c => c.Id == id);
            if (coverType != null)
            {
                _coverTypeRep.Remove(coverType);
                _dbRep.Save();
                TempData["success"] = "CoverType deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return NotFound();
            }
            return View(coverType);
        }

    }
}
