﻿using EBooking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBooking.DataAccess
{
    public class ProductRepository : EntityBaseRepository<Product>, IProductRepository
    {
        private AppDbContext _db;

        public ProductRepository(AppDbContext db) : base(db)
        {
            _db = db;
        }
        public void Update(Product obj)
        {
            //  _db.Products.Update(obj); 

            var productFromDB = _db.Products.FirstOrDefault(x=> x.Id == obj.Id);
            if (productFromDB != null)
            {
                productFromDB.Title = obj.Title;
                productFromDB.ISBN = obj.ISBN;
                productFromDB.Description=obj.Description;
                productFromDB.Author=obj.Author;
                productFromDB.ListPrice = obj.ListPrice;
                productFromDB.Price = obj.Price;    
                productFromDB.Price50=  obj.Price50;
                productFromDB.Price100= obj.Price100;
                productFromDB.CategoryId=obj.CategoryId;
                productFromDB.CoverTypeId=obj.CoverTypeId;
                if(obj.ImageUrl!=null)
                {
                   productFromDB.ImageUrl = obj.ImageUrl;   
                }

            }
        }
    }
}
