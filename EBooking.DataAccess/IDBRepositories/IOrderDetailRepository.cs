﻿using EBooking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBooking.DataAccess
{
    public interface IOrderDetailRepository : IEntityBaseRepository<OrderDetail>
    {
        void Update(OrderDetail obj);
    }
}
