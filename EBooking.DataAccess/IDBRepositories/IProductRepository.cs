﻿using EBooking.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBooking.DataAccess
{
    public interface IProductRepository : IEntityBaseRepository<Product>
    {
        void Update(Product obj);
    }
}
